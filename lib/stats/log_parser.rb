# frozen_string_literal: true

module Stats
  class LogParser
    PARSER_NAMESPACE = 'Stats::Parsers'

    private_constant :PARSER_NAMESPACE

    attr_reader :path, :options

    def initialize(options)
      @path = options.delete(:path)
      @options = options
    end

    def parse
      parser_class.new(lines).results
    end

    private

    def parser_class
      Object.const_get("#{PARSER_NAMESPACE}::#{demodulized_parser_class}")
    end

    def demodulized_parser_class
      (options.keys.first || 'pages').capitalize
    end

    def lines
      @lines ||= read_file.split("\n").lazy
    end

    def read_file
      File.read(path)
    end
  end
end
