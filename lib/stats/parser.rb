# frozen_string_literal: true

module Stats
  class Parser
    attr_reader :lines, :map

    def initialize(lines)
      @lines = lines
      @map = {}
    end

    def results; end

    private

    def output
      sorted.each do |result|
        puts "#{result.first} #{result.last} visits"
      end
    end

    def sorted
      map.sort_by { |_k, v| v }
    end
  end
end
