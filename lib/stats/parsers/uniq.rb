# frozen_string_literal: true

module Stats
  module Parsers
    class Uniq < Stats::Parser
      def results
        parse
        output
      end

      private

      def parse
        helper_map = {}

        lines.each do |line|
          url, ip = line.split

          helper_map[url] ||= {}

          unless helper_map[url][ip]
            visits = map.fetch(url, 0)
            map[url] = visits + 1
          end

          helper_map[url][ip] = true
        end
      end
    end
  end
end
