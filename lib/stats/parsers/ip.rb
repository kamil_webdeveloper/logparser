# frozen_string_literal: true

module Stats
  module Parsers
    class Ip < Stats::Parser
      def results
        parse
        output
      end

      private

      def parse
        lines.each do |line|
          _, ip = line.split
          visits = map.fetch(ip, 0)
          map[ip] = visits + 1
        end
      end
    end
  end
end
