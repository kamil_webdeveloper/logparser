# frozen_string_literal: true

module Stats
  module Parsers
    class Pages < Stats::Parser
      def results
        parse
        output
      end

      private

      def parse
        lines.each do |line|
          url, = line.split
          visits = map.fetch(url, 0)
          map[url] = visits + 1
        end
      end
    end
  end
end
