# frozen_string_literal: true

require 'zeitwerk'
require 'optparse'
require 'pry'

module Stats
  def self.boot
    Zeitwerk::Loader.new
                    .tap { |l| l.push_dir(File.absolute_path(__dir__)) }
                    .tap(&:setup)
                    .tap(&:eager_load)
  end

  def self.generate(options)
    Stats::LogParser.new(options).parse
  end
end
