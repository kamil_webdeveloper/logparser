# README #

Smart Pension Task

### Usage ###

/bin/stats.rb path/to/webserver.log

### Options ###

* -u uniq visits
* -ip by ip
* -h  help

### Examples ###

/bin/stats.rb path/to/webserver.log -u

/bin/stats.rb path/to/webserver.log -ip