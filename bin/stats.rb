#!/usr/bin/env ruby
# frozen_string_literal: true

require File.join(File.expand_path('../', __dir__), 'lib/stats.rb')

options = { path: ARGV.first }

OptionParser.new do |opt|
  opt.on('-u', '--uniq') { |_o| options[:uniq] = true }
  opt.on('-ip', '--by-user-ip') { |_o| options[:ip] = true }
end.parse!

Stats.boot
Stats.generate(options)
