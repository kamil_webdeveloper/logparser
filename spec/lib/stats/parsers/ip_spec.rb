# frozen_string_literal: true

require 'spec_helper'

# rubocop:disable Metrics/BlockLength
describe Stats::Parsers::Ip do
  let(:log_file_path) { File.join(File.expand_path('../../../', __dir__), 'files/webserver.log') }
  let(:lines) { File.read(log_file_path).split("\n") }
  let(:pages) do
    %w[
      929.398.951.889 217.511.476.080 715.156.286.412 802.683.925.780 555.576.836.194 126.318.035.038
      235.313.352.950 897.280.786.156 646.865.545.408 061.945.150.735 682.704.613.213 336.284.013.698
      200.017.277.774 444.701.448.104 016.464.657.359 836.973.694.403 316.433.849.805 543.910.244.929
      382.335.626.855 184.123.665.067 451.106.204.921 158.577.775.616 722.247.931.582
    ]
  end
  let(:visits) { [12, 14, 16, 17, 18, 18, 19, 19, 19, 19, 20, 21, 21, 21, 22, 25, 25, 27, 27, 29, 29, 31, 31] }

  subject { described_class.new(lines) }

  describe '#results' do
    it 'generate ordered map of page visits' do
      subject.results
      sorted_map = subject.send(:sorted)

      expect(sorted_map).to be_a(Array)
      expect(sorted_map.map(&:first)).to eq(pages)
      expect(sorted_map.map(&:last)).to eq(visits)
    end

    it 'print out results' do
      printoutes = [
        '929.398.951.889 12 visits',
        '217.511.476.080 14 visits',
        '715.156.286.412 16 visits',
        '802.683.925.780 17 visits',
        '555.576.836.194 18 visits',
        '126.318.035.038 18 visits',
        '235.313.352.950 19 visits',
        '897.280.786.156 19 visits',
        '646.865.545.408 19 visits',
        '061.945.150.735 19 visits',
        '682.704.613.213 20 visits',
        '336.284.013.698 21 visits',
        '200.017.277.774 21 visits',
        '444.701.448.104 21 visits',
        '016.464.657.359 22 visits',
        '836.973.694.403 25 visits',
        '316.433.849.805 25 visits',
        '543.910.244.929 27 visits',
        '382.335.626.855 27 visits',
        '184.123.665.067 29 visits',
        '451.106.204.921 29 visits',
        '158.577.775.616 31 visits',
        '722.247.931.582 31 visits'
      ]

      printoutes.each { |p| expect($stdout).to receive(:puts).with(p) }
      subject.results
    end
  end
end
# rubocop:enable Metrics/BlockLength
