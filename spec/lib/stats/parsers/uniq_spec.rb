# frozen_string_literal: true

require 'spec_helper'

describe Stats::Parsers::Uniq do
  let(:log_file_path) { File.join(File.expand_path('../../../', __dir__), 'files/webserver.log') }
  let(:lines) { File.read(log_file_path).split("\n") }
  let(:pages) { %w[/about /about/2 /help_page/1 /contact /home /index] }
  let(:visits) { [21, 22, 23, 23, 23, 23] }

  subject { described_class.new(lines) }

  describe '#results' do
    it 'generate ordered map of uniq page visits' do
      subject.results
      sorted_map = subject.send(:sorted)

      expect(sorted_map).to be_a(Array)
      expect(sorted_map.map(&:first)).to eq(pages)
      expect(sorted_map.map(&:last)).to eq(visits)
    end

    it 'print out results' do
      expect($stdout).to receive(:puts).with('/about 21 visits')
      expect($stdout).to receive(:puts).with('/about/2 22 visits')
      expect($stdout).to receive(:puts).with('/help_page/1 23 visits')
      expect($stdout).to receive(:puts).with('/contact 23 visits')
      expect($stdout).to receive(:puts).with('/home 23 visits')
      expect($stdout).to receive(:puts).with('/index 23 visits')

      subject.results
    end
  end
end
