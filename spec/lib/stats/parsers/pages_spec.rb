# frozen_string_literal: true

require 'spec_helper'

describe Stats::Parsers::Pages do
  let(:log_file_path) { File.join(File.expand_path('../../../', __dir__), 'files/webserver.log') }
  let(:lines) { File.read(log_file_path).split("\n") }
  let(:pages) { %w[/home /help_page/1 /about /index /contact /about/2] }
  let(:visits) { [78, 80, 81, 82, 89, 90] }

  subject { described_class.new(lines) }

  describe '#results' do
    it 'generate ordered map of page visits' do
      subject.results
      sorted_map = subject.send(:sorted)

      expect(sorted_map).to be_a(Array)
      expect(sorted_map.map(&:first)).to eq(pages)
      expect(sorted_map.map(&:last)).to eq(visits)
    end

    it 'print out results' do
      expect($stdout).to receive(:puts).with('/home 78 visits')
      expect($stdout).to receive(:puts).with('/help_page/1 80 visits')
      expect($stdout).to receive(:puts).with('/about 81 visits')
      expect($stdout).to receive(:puts).with('/index 82 visits')
      expect($stdout).to receive(:puts).with('/contact 89 visits')
      expect($stdout).to receive(:puts).with('/about/2 90 visits')

      subject.results
    end
  end
end
