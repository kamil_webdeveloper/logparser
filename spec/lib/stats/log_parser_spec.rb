# frozen_string_literal: true

require 'spec_helper'

# rubocop:disable Metrics/BlockLength
describe Stats::LogParser do
  let(:path) { 'test/path' }
  let(:args) { { uniq: true } }
  let(:options) { { path: path }.merge(args) }
  subject { described_class.new(options) }

  describe '#new' do
    it 'set readers for path and options parser' do
      expect(subject.path).to eq(path)
      expect(subject.options).to eq(args)
    end
  end

  describe '#parse' do
    let(:parser) { instance_double(Stats::Parser, results: '') }

    before { allow(subject).to receive(:lines).and_return([]) }

    context 'without options' do
      let(:options) { { path: path } }

      it 'call page parser' do
        expect(Stats::Parsers::Pages).to receive(:new).and_return(parser)
        subject.parse
      end
    end

    context 'with ip option' do
      let(:options) { { path: path, ip: true } }

      it 'call ip parser' do
        expect(Stats::Parsers::Ip).to receive(:new).and_return(parser)
        subject.parse
      end
    end

    context 'with uniq option' do
      let(:options) { { path: path, uniq: true } }

      it 'call uniq parser' do
        expect(Stats::Parsers::Uniq).to receive(:new).and_return(parser)
        subject.parse
      end
    end

    context 'with few options' do
      let(:options) { { path: path, ip: true, uniq: true } }

      it 'call first option parser' do
        expect(Stats::Parsers::Ip).to receive(:new).and_return(parser)
        subject.parse
      end
    end
  end

  describe '#lines' do
    let(:file) { double(:file, split: []) }

    it 'read file with path' do
      expect(File).to receive(:read).with(path).and_return(file)
      subject.send(:lines)
    end

    let(:lines_count) { 4 }
    let(:file_string) { "string\n" * lines_count }

    it 'returns array of lines' do
      allow(subject).to receive(:read_file).and_return(file_string)
      expect(subject.send(:lines)).to be_a(Enumerator::Lazy)
      expect(subject.send(:lines).size).to eq(lines_count)
    end
  end
end
# rubocop:enable Metrics/BlockLength
