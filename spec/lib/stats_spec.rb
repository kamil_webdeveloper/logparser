# frozen_string_literal: true

require 'spec_helper'
require 'pry'

describe Stats do
  let(:options) { { path: '/example/path' } }
  let(:log_parser_double) { instance_double(described_class::LogParser) }

  describe '#generate' do
    it 'Initialize parser' do
      expect(Stats::LogParser).to receive(:new).with(options).and_return(log_parser_double)
      expect(log_parser_double).to receive(:parse)
      described_class.generate(options)
    end
  end
end
